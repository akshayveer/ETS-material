1998��5�� ��������

1. The view is spectacular. Could you take a picture of me with the mountains in the background?
I'm afraid I just ran out of film 
What does the woman mean?  (C)

2. Excuse me, were you ready to order now?
I'll be with you in just minute.
What does the man mean?  (B)

3. I think I forgot my umbrella. Did you notice that it was raining outside?
Yeah. It is. And I just realize that I left my car window open.
What will the man probably do next?  (C)

4. How does your daughter like her new school?
Fine. She seems to have made new friends in no time.
What can be inferred about the man's daughter?  (C)

5. There is an article here in this magazine you might interesting. It's about buying running shoes. 
If it's not chemistry and it's not on the final exam, I can't read it now.
What is the man probably doing?  (B)

6. You are washing your car even on vacation. It makes me feel guilty.
You shouldn't. It's just that I have nothing better to do at the moment.
What does the woman imply?  (C)

7. My doctor told me I needed to go for some expensive treatment for my injured knee.
Are you sure? Maybe you need a second opinion.
What does the woman suggest the man do?  (C)

8. Hello, could me fit me in for an appointment today? I need a stylist who's good at cutting curly hair.
Judy is good at that. She is the owner. How about the noon?
What will the woman probably do?  (B)

9. There is nothing I like more than a good mystery novel when I've got some spare time.
I like to read too. But I prefer non-fiction: history, social commentary and stuff like that.
What does the man imply?  (C)

10. I'm no expert. But that noise in your refrigerator doesn't sound good.
Maybe you should call and have it checked out.
You are right. And I suppose I've put it off long enough.
What will the woman probably do?   (A)

11. What's up with Donald? I've never seen him so happy.
   His supervisor gave performance evaluations this morning.
   What can be inferred about Donald?  (A)

12. It's not going to snow again tomorrow, is it? It was supposed to be warm all week.
Well, if you go by the forecast that I heard, you shouldn't put your coat and hat away quite yet.
What does the woman imply?  (B)

13. I don't know how it happened. But I got two different appointments at lunch tomorrow. I'm supposed to meet both David and Jim.
   Why don't you make one of them a breakfast meeting?
   What does the woman suggest the man do?  (B)

14. It's all right to wear jeans for a class presentation, isn't it?
That's what I'm wearing. But if we wear jackets too, maybe we won't look so casual.
What does the man imply?  (C)

15. This spring weather is perfect for playing tennis.
Unfortunately the only time I get to enjoy it is when I'm walking to class or to the library.
What does the man imply?  (D)

16. Boy, how quickly technology changes. So many people have a computer in their home nowadays.
I know. I feel so behind the time.
What can be inferred about the woman?  (D)

17. Joan and her friend went to new restaurant last night and said that it served the best food they ever had.
That's quite a recommendation. Maybe we should see for ourselves.
What will the speakers probably do?  (D)

18. I hear you have a brother who went to school here too. Have I ever seen him?
Well, he graduated last year. But you would never have guessed that we were brothers.
What does the man imply?  (D)

19. I'm thinking of heading to the gym before going to dinner. Care to join me?
If you don't mind waiting while I go get my gym bag.. 
What does the man mean?  (B)

20. I'm in a terrible mood. My boss didn't like the report I wrote.
Well, don't take it out on me.
What does the man mean?  (A)

21. So I guess it's been a year now since your last checkup. Have you had any health problems?
None to speak of.
What does the woman mean?  (B)

22. Have you ordered your graduation announcements?
No, I had Don do it for me.
What does the woman say about the graduation announcements?  (D)

23. I hear you got a big parking ticket.
Yeah. I never realized Lot 3 was only for faculty.
What does the man imply?  (C)

24. I went through a whole box of paper and a printer ribbon just trying to get my resume right.
It'll be worth it. You know just to make a good impression.
What can be inferred from the conversation?  (B)

25. So you weren't happy with the way the newspaper covered the rally protesting the rising tuition fees?
No. The article underestimated the number of students who were there and I don't think it explained our point of view very well.
What can be inferred about the man?  (A)

26. When are you ever going to finish this report? You've been working on it for three months.
Only two and half. But it does seem longer.
What does the woman say about the report?  (D)

27. I've been meaning to get my eyes checked. I just haven't gotten around to it yet.
Why don't you call for an appointment right away? Once on your calendar you will get it done. What does the man suggest the woman do?  (B)

28. Hey, Lisa. Has Professor Smith returned my call yet?
I just got in a little while ago myself.
What does Lisa imply?  (C)

29. I got an invitation to a financial planning seminar. And I don't want to go alone.
Count me in. I need all the help I can get managing my money.
What will the woman probably do?  (B)

30. I hope you are not too put out with me for stopping by Fred's on the way over here. I had to pick up an assignment.
Well, that's not a big deal. But you might at least phone if you know you are going to keep someone waiting.
What does the woman mean?  (C)

PART B
31-34 Conversation at a registrar's office at a university.
* I want to register for this mathematics course.
* I'm sorry registration has closed. 
* Closed? The clerk told me I could come back and register any time during the first week of classes.
* Well, that's not possible. The computer's official student account has already been sent to the state. And that's what our budget is based on. Who told you that anyway?
* Some woman here when I tried to register three weeks ago. She said I just had to pay a late fee.
* She must have been a temporary worker. They don't have much training. Why didn't you register then?
* She said I couldn't until I had my birth certificate. Here it is. 
* Your birth certificate?
* Well, I'm a new part-time student. So she ask for identification. I don't drive so I don't have a driver's license.
* Huh. That's no reason to demand a birth certificate. We only need to establish residency: a phone bill with your name and address on it would've been fine.
* Really? Only prove of my address?
* Yes. I'm afraid she gave you the wrong information. Still you'll have to wait and take your math's class next semester.
* But that's on it fair.
* Well, I sympathize with your problem, but frankly, I don't think there is anything anyone can do for you. You were trapped in the system. If you want to you can talk to the director. She will hilp you if she can.
* Great.
* Don't get your hopes up.

31. What problem does the woman have?  (B)
32. Why does the woman have to go to the office two times?  (D)
33. According to the man, what does the woman need to show the evidence of?  (B)
34. Why does the man imply when he tells the woman "not to get her hopes up"?  (A)

35-38 Conversation between two students doing chemistry experiment.
* Hi, Mary. Do you want to start writing a lab report after we finish this experiment?
* I can't. In fact I need to finish early because I'm going over to the psychology department to talk to Professor Smith about a job opening.
* You mean a job on campus?
* Yeah. And it sounds pretty interesting. It involves helping with your study on learning style. You know, about how some people learn best by sight, while others learn best by hearing or touch.
* Yeah. I know that's an area of expertise.
* Right. Anyway for her study she's taking some high school students who aren't doing very well in their classes and testing them to find out what their learning styles are. Then tutors, people like me, will work with them presenting material to them in their particular learning style.
* Hey. That is interesting. Now will you mostly do the testing or the tutoring?
* Both I hope. I want to be involved from start to finish.
* Are you getting paid for this?
* I'm sure we'll get something though, probably not much. Anyway it doesn't matter to me, I just want to have some hands-on experience.
* Yeah. And it'll be nice to help those high school students too. 
* That's what I thought when I saw the ad. You know you could do it too. You don't have to be in her classes to work on the study.
* Really? Do you have any idea what the schedule is like?
* Late afternoon then evening for tutoring I think. After all the kids are in regular classes until three thirty.
* Actually that's perfect for me. 
* Then come along. We will save the lab report for later. But we'd better make sure we do a good job on our experiment first.
* Yeah. First thing's first.
35. What are the speakers mainly discussing?  (D)
36. Why is the woman interested in working with Professor Smith?  (B)
37. What will the college students do for the high school students?  (C)
38. What will the speakers probably do next?  (D)

PART C
39 to 42 A museum guide talking about native American pottery.
Hello, I'll be your tour guide today here at the art museum so I'd like to welcome you to this month's exhibit of native American pottery. We'll begin our tour in a few minutes. But first I'm going to tell you something about the way this pottery was created. Pottery was made all over ancient North America by many different groups of people. One of the earliest of these ancient American  cultures was the Hohokam people. They lived in what is now Arizona from about 300 BC to AD 1500. And it's their pottery that you will be looking at today. All of the pottery was made from clay. Some objects were mug, bowls and ladies for drinking and eating. You will also see finger rings and animal-shaped incense burners which we believe were probably used in special ritual. The Hohokam formed their pottery vessels from coils of clay. Then shaped them with special tools to create very thin sides on the vessels. Afterwards they painted the pottery with red design. Actually many of the pieces here have designs right on them that show how the pottery was used. Now I hope you'll enjoy the beauty and the uniqueness of the Hohokam pottery and that will give you some interesting insights about the people who created it. Please feel free to ask me any questions and thank you for joining us today.

39. What does the speaker mainly discuss?  (C)
40. What is the purpose of the talk?  (B)
41. What did the Hohokam do with their pottery?  (B)
42. What does the speaker say about the way the hohokam pottery was made?  (A)

43 to 46 A talk in an American history class.
I'm going to introduce two current points of view about the motivation for writing the United States Constitution back in 1787. The first one is called the idealist view. The idealists basically believe that the writers of the Constitution were motivated by ideas. Which ideas? The ideas of the revolutionary war, such as liberty and democracy. The idealists remind that the young country had a lot of problems: an economic depression, a large war debts, lawlessness and trade barriers between the states. They argue that the representatives needed to control these problems in order for the United States to survive. The other point of view is the economic view. The economic view is that the writers of the Constitution were concerned about their own financial interests. According to them most people were living wealth for the wealthiest people were afraid of losing their money. The writers wanted a strong central government that would promote trade protect private property and perhaps most of all collect taxes to pay off the United States' large war debts. Because a number of those who wrote the constitution had loaned money to the government during the revolution. Which view is correct? Well historians who wrote during the calm and prosperous 1950s found reasons to believe the idealist view. Those who wrote during the trouble of 1960s found support for the economic view point. I'd say that neither view is complete, both the idealist and the economic perspective contribute a part to the whole picture.

43. What is the talk mainly about?  (D)
44. According to the economic view, who benefited the most from the new Constitution?  (B)
45. What can be inferred about the views of the historians ?  (C)

46 to 50 Part of a speech on birds by a biologist.
Many egg-laying animals merely lay their eggs and leave. Turtles for instance, the eggs hatch on their own. The current theory about birds is that the earliest birds did just that when they were cold-blooded creatures living in warm places. However when they became warm-blooded creatures living in cold places they had to remain on the eggs to keep them warm. The process we call incubation. For this they needed a place a nest. Very likely the first nests were just primitive depressions scrape into the ground. Even now many species still lay eggs in this sort of crude nests. In fact every spring a mother killdeer lays her in some pebbles along the edge of the parking lot just outside this building. Primitive nests on the ground were fine for some birds but others began to elevate their nests in branches perhaps to avoid predators. These early elevated nests were probably loose platforms of sticks and twigs the types still built by ospreys and mostarians today. The latest evolvement in nest the most recent version, so to speak, is the cup-shaped nest. This is the one we regard today as the typical bird's nest, you know, like a robin's nest.

46. What is the main topic of the talk?  (C)
47. What evolutionary change in birds led to nest building?  (B)
48. According to the speaker, where were the first bird's nests located?  A)
49. What is the killdeer's nest in parking lot an example of?  (A)
50. According to the speaker, what is a possible reason that birds began to build nests in trees?

